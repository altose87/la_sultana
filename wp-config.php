<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'la_sultana');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '5AXB+! {gHxwgf13bY87W>KGW!|+QVhlJ.^iLYjsK$Rd1laBH0k~k!;%UAt3~E_n');
define('SECURE_AUTH_KEY', 'ArObjk7s} ;MQ#:b_A`TIL/P|2>N+-9x^U:Bu;|(zWx@[zipV>oN)gX4i-2!mRX5');
define('LOGGED_IN_KEY', 'I-;e$~M{z:yKG&+EP^1kwQ#eNDCd8(A%t(y4g>?~C4{N42zuLrTu|Z/_jm-|(pOt');
define('NONCE_KEY', 'T;4kW_:6#sCRv~%Uwk IqgV3BsKd+x?COB#UaGDZj|qu6Zt6mtSF@]e[i,k$jQUv');
define('AUTH_SALT', 'y&F6n!o^A70jFC@fo|a_!_k+r+g.R3#tRg)0XVkx-|3#:x@iMcI+lWKk1;%p=aFN');
define('SECURE_AUTH_SALT', '}MgZ@i+9h@5?i7|5pyn@HL/P$(Xx_exq@H+vu6udXHY6d0{*G^u2zA-c@oPIb7|%');
define('LOGGED_IN_SALT', 'aIMoG|JWoLi#Wy-j+p)z-Sj&kSZ*z&FQsYp*T:?i{$8r_rhfC},5e?7-7~y.y:y|');
define('NONCE_SALT', 'u1nF$ahz+HYO|o #nRHwM6-u^WyD15dEcDY]T%E$+yDk(.C&HE sbpzT01-|zlOk');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

